import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet} from 'react-native';

let styles;

/**
 * Creates a wide rounded corner button
 * Takes 7 props :
 *
 * title : Text displayed on the button
 * icon  : icon on the left of the button
 * backgroundColor : button background color
 * textColor : color of the title text
 * borderWidth: thickness of border
 * borderColor: color of border
 * onPress : calls function on button press (no parameters passed)
 * disabled: disables button
 */
export default class RoundButton extends Component {
  render() {
    return (
      <View {...this.props.containerProps}>
        <TouchableOpacity
          style={[
            styles.buttonStyle,
            {
              backgroundColor: this.props.backgroundColor,
              borderWidth: this.props.borderWidth,
              borderColor: this.props.borderColor,
            },
          ]}
          onPress={this.props.onPress}
          disabled={this.props.disabled}>
          <View style={styles.iconContainer}>
            <Image style={styles.iconStyle} source={this.props.icon} />
          </View>

          <Text style={[styles.titleStyle, {color: this.props.textColor}]}>
            {this.props.title}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

styles = StyleSheet.create({
  buttonStyle: {
    borderRadius: 125,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginBottom: 20,
  },

  iconContainer: {
    position: 'absolute',
    left: 13,
  },

  iconStyle: {
    margin: 5,
    alignSelf: 'flex-start',
  },

  titleStyle: {
    fontSize: 14,
    letterSpacing: 1.39,
    lineHeight: 24,
    paddingStart: 25,
    paddingEnd: 25,
    textAlign: 'center',
  },
});
