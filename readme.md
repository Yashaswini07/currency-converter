# Currency Converter

A simple and beautiful Currency Converter built using react native. Built using existing react native components.It uses api.openrates.io to fetch the latest rates.

## Getting Started

### Prerequisites

Setup react native using the official documentation. [facebook.github.io](https://facebook.github.io/react-native/docs/getting-started)

### Installing

1.  clone the repository
2.  run `npm install` in the repository directory

### Build and Run

`react-native run-ios`

or

`react-native run-android`

## Screenshots

![Main Screen](./Screenshots/iPhoneX.png)
![Main Screen](./Screenshots/oneplus.png)
