/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment, Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput,
  StatusBar,
  TouchableOpacity,
  Image,
} from 'react-native';
import {Dropdown} from 'react-native-material-dropdown';

import {Colors} from 'react-native/Libraries/NewAppScreen';
import countries from './countries';
import RoundButton from './RoundButton';
import rev from './reverse.png';

const loadingGif = require('./loading.gif');

export default class CurrencyConverter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: null,
      fromCurrency: '',
      toCurrency: '',
      amount: 0,
      currencies: [],
      convert: false,
      rate: '',
      error: '',
      isLoading: false,
    };
    fetch('http://api.openrates.io/latest')
      .then(response => response.json())
      .then(response => {
        const currencyAr = [];
        for (const key in response.rates) {
          currencyAr.push({value: key});
        }
        this.setState({
          currencies: currencyAr.sort((a, b) => a.value.localeCompare(b.value)),
        });
      })
      .catch(err => {
        console.log('Oops', err.message);
      });
  }

  // Event handler for the conversion
  convertHandler = () => {
    if (this.state.fromCurrency && this.state.toCurrency) {
      this.setState({isLoading: true});
      fetch(
        `http://api.openrates.io/latest?base=${
          this.state.fromCurrency
        }&symbols=${this.state.toCurrency}`,
      )
        .then(response => response.json())
        .then(response => {
          const result =
            this.state.amount * response.rates[this.state.toCurrency];
          this.setState({
            result: result.toFixed(5),
            rate: response.rates[this.state.toCurrency],
            convert: true,
            error: '',
            isLoading: false,
          });
        })
        .catch(err => {
          console.log('Oops', err.message);
        });
    } else {
      this.setState({error: 'Please select both countries to convert'});
    }
  };

  //Function to reverse the currencies
  reverse = () => {
    this.setState(
      {
        fromCurrency: this.state.toCurrency,
        toCurrency: this.state.fromCurrency,
        convert: false,
      },
      () => this.convertHandler(),
    );
  };

  render() {
    return (
      <View style={{flex: 1, backgroundColor: Colors.lighter}}>
        <Fragment>
          <StatusBar barStyle="dark-content" />
          <SafeAreaView>
            <ScrollView
              contentInsetAdjustmentBehavior="automatic"
              style={styles.scrollView}>
              <View style={styles.body}>
                <View style={styles.content}>
                  <View style={styles.titleContainer}>
                    <Text style={styles.title}>Currency Converter</Text>
                  </View>
                  <View style={styles.sectionContainer}>
                    <TextInput
                      style={styles.amountText}
                      onChangeText={amount =>
                        this.setState({amount: Number(amount)})
                      }
                      editable
                      keyboardType={'number-pad'}
                      value={String(this.state.amount)}
                    />
                    <Dropdown
                      label="Select Country"
                      data={this.state.currencies}
                      onChangeText={value =>
                        this.setState({fromCurrency: value, convert: false})
                      }
                      labelExtractor={item => countries[item.value]}
                      value={this.state.fromCurrency}
                    />
                  </View>
                  <View style={styles.sectionContainer}>
                    <TouchableOpacity
                      style={{alignItems: 'center'}}
                      onPress={this.reverse}>
                      <Image source={rev} style={{width: 30, height: 30}} />
                    </TouchableOpacity>
                  </View>
                  <View style={styles.sectionContainer}>
                    <View style={styles.result}>
                      <Text onChangeText={result => this.setState({result})}>
                        {this.state.result}
                      </Text>
                    </View>
                    <Dropdown
                      label="Select Country"
                      data={this.state.currencies}
                      onChangeText={value =>
                        this.setState({toCurrency: value, convert: false})
                      }
                      labelExtractor={item => countries[item.value]}
                      value={this.state.toCurrency}
                    />
                  </View>
                  <View style={styles.sectionContainer}>
                    <RoundButton
                      title="CONVERT"
                      backgroundColor="blue"
                      textColor="white"
                      onPress={this.convertHandler}
                      containerProps={{paddingStart: 40, paddingEnd: 40}}
                    />
                    {this.state.convert ? (
                      <Text style={styles.infoText}>
                        {'1'} {this.state.fromCurrency} = {this.state.rate}{' '}
                        {this.state.toCurrency}
                      </Text>
                    ) : null}
                    <Text
                      style={[
                        {
                          color: 'red',
                        },
                        styles.infoText,
                      ]}>
                      {this.state.error}
                    </Text>
                  </View>
                  {this.state.isLoading ? (
                    <View style={styles.loader}>
                      <Image source={loadingGif} style={styles.loadImg} />
                    </View>
                  ) : null}
                </View>
              </View>
            </ScrollView>
          </SafeAreaView>
        </Fragment>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
  },
  sectionContainer: {
    marginTop: 30,
    paddingHorizontal: 24,
  },
  titleContainer: {
    marginTop: 30,
    paddingHorizontal: 24,
    height: 40,
  },
  content: {
    flex: 1,
    justifyContent: 'center',
  },
  infoText: {
    textAlign: 'center',
    fontSize: 16,
  },
  result: {
    height: 40,
    borderColor: 'black',
    borderWidth: 1,
    alignSelf: 'stretch',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  amountText: {
    height: 40,
    borderColor: 'black',
    borderWidth: 1,
    paddingHorizontal: 10,
  },
  title: {
    fontSize: 24,
    color: 'blue',
    textAlign: 'center',
  },
  loader: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    elevation: 10,
    zIndex: 10,
  },
  loadImg: {
    width: 100,
    height: 100,
    position: 'absolute',
    alignSelf: 'center',
  },
});
